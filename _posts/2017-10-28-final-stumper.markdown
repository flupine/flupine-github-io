---
title: "Final Stumper"
layout: post
date: 2017-10-28 22:10
tag: piscine
headerImage: true
projects: true
hidden: true # don't count this post in blog pagination
description: "Pattern recognition"
category: project
author: flupine
externalLink: false
---

This project was actually rush03 (and last) of my pool, it was to find the pattern of any rush done 2 weeks before. 
You can consult the project code on this page: [GitHub](https://github.com/flupine/Final_Stumper)

How it works :
<script type="text/javascript" src="https://asciinema.org/a/q7mbvmVSGD7KNeCIPCPqVn7Q9.js" id="asciicast-q7mbvmVSGD7KNeCIPCPqVn7Q9" async></script>