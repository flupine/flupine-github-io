---
title: About
layout: page
---
![Profile Image]({{ site.url }}/{{ site.picture }})

<p>Since a few years now, I am interested in software development but also in security, I love to discover and learn new things, that's why I went to a school of computer expertise such as epitech. In addition to my school projects, I work on some sides projects that you can find on this site. I am also a fervent defender of open source software, freedom on the internet and I am interested in the operation of cryptocurrencies such as trading, mining ...</p>

<h2>Skills</h2>

<ul class="skill-list">
	<li>HTML</li>
	<li>CSS (Stylus, Sass, Less)</li>
	<li>Ruby</li>
	<li>C</li>
	<li>Linux(Centos, Debian, Arch)</li>
	<li>Git</li>
	<li>PHP</li>
	<li>Python</li>
	<li>MySQL</li>
	<li>Trello</li>
	<li>Continuous Integration</li>
</ul>

<h2>Projects</h2>

<ul>
	<li><a href="https://github.com/EriumCoin">EriumCoin</a></li>
	<li><a href="https://github.com/flupine/libmy">Libmy</a></li>
	<li><a href="https://github.com/flupine/101Pong">101Pong</a></li>
	<li><a href="https://github.com/flupine/Final_Stumper">Final Stumper</a></li>
	<li><a href="https://github.com/flupine/Piscine">Pool 2017</a></li>
</ul>
